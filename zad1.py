def read_file(file_name):
    with open(file_name, 'rb') as file:
        data = file.read()
    return data

def save_to_file(file_name, data):
    with open(file_name, 'wb') as file:
        data = file.write(data)
    return data

def corrupt_data(data):
    import random
    number_of_corruption = random.randint(0, len(data) - len(data)//2) #how many bits to corrupt
    data = list(data)
    for _ in range(number_of_corruption):
        random_index = random.randint(0, len(data) - 1) #select random bit
        data[random_index] = random.choice(['0','1']) #change bit to 0 or 1
    return ''.join(data) 

def generate_big_file(file_name, size):
    import os
    with open(file_name, 'wb') as fout:
        fout.write(os.urandom(size)) #1
    print(f"generated file {file_name} with size {size}")

def convert(data, base2=False):
    tmp = bytearray(data)
    array_of_bytes = [bin(tmp[i])[2:] for i in range(len(tmp))]
    to_string = ''.join(array_of_bytes)
    if base2:
        return to_string
    print(to_string)
    return int(to_string, 2)

def parity(data):
    bit = 0
    num_bits = 0
    while data:
        bitmask = 1 << bit
        bit += 1
        if data & bitmask:
            num_bits += 1
        data &= ~bitmask
    return num_bits % 2

def modulo(data1, data2):
    return bytes([x ^ y for x, y in zip(data1, data2)])

def crc(data, n, divider, check=False, check_value=''):
    #data = '10110101101101'
    #n = 3
    #n+1 = '1101'
    n_bit_crc = ''.join(['0' for _ in range(n)]) #const, n ostatnich bitów
    if check:
        n_bit_crc = check_value
    iterate = True
    data = data + n_bit_crc
    #calculate reminder
    while iterate:
        n_bits = data[:(n+1)]
        #print(f"n_bits: {n_bits}\n")
        #print(f"divider: {divider}\n")
        modulo_result = int(n_bits, 2) ^ int(divider, 2) # xor n first bits
        modulo_result = bin(modulo_result)[2:] # convert to binary string '0b...'
        #print(f"modulo_result: {modulo_result}\n")
        data = modulo_result + data[(n+1):] # (string addition) swap n+1 first bits with modulo result
        #print(f"data after modulo: {data}\n")
        data = data.lstrip('0') # remove leading 0
        #print(f"stripped data: {data}\n")
        if len(data) < len(divider):
            reminder = n_bit_crc[:(n-len(data))] + data
            if check:
                return data
            iterate = False
            return reminder

def test_parity(file_name):
    x = convert(read_file(file_name))
    return parity(x)

def test_modulo(file_name, data2=b'abcdefghijk'):
    data1 = read_file(file_name)
    return convert(modulo(data1, data2))

def test_crc(file_name, corrupt=False):
    divider = input("crc divider: ")
    n = len(divider) - 1
    data = convert(read_file(file_name), base2=True)
    reminder = crc(data, n, divider)
    print(f"crc reminder: {reminder}")
    if corrupt:
        crc_check = crc(corrupt_data(data), n, divider, check=True, check_value=reminder)
    else:
        crc_check = crc(data, n, divider, check=True, check_value=reminder)
    if not crc_check: # if reminder = 0, crc check is a success
        print(f"Success")
    else:
        print(f"Failed")


if __name__ == "__main__":
    file_name = "text.txt"
    # 1 - odd, 0 - even
    # print(test_parity(file_name))
    # print(test_modulo(file_name))
    test_crc(file_name, corrupt=True)
    #print(crc_reminder('1110110101111110010101011', 4))
